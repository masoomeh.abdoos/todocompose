package com.example.to_docompose.di

import android.content.Context
import androidx.room.Room
import com.example.to_docompose.data.ToDoDataBase
import com.example.to_docompose.data.repository.FakeToDoRepository
import com.example.to_docompose.data.repository.ToDoRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Named
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object TestAppModule {

    @Provides
    @Named("DataBaseTest")
    fun injectInMemoryRoom(@ApplicationContext context: Context) =
        Room.inMemoryDatabaseBuilder(context,ToDoDataBase::class.java)
             .allowMainThreadQueries()
            .build()

    @Provides
    @Singleton
    @Named("fakeToDoRepo")
    fun provideToDoRepository(): ToDoRepository {
        return FakeToDoRepository()
    }


//    @Provides
//    @Singleton
//    @Named("fakeDataStoreRepo")
//    fun provideDataStoreRepository(): DataStoreRepository {
//        return FakeDataStoreRepository()
//    }
}
