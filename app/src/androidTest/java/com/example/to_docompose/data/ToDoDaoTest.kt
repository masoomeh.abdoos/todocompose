package com.example.to_docompose.data

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.SmallTest
import app.cash.turbine.test
import com.example.to_docompose.data.model.Priority
import com.example.to_docompose.data.model.ToDoTask
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.CoroutineExceptionHandler
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.launch
import kotlinx.coroutines.test.runTest
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject
import javax.inject.Named

@RunWith(AndroidJUnit4::class)
@SmallTest
@ExperimentalCoroutinesApi
@HiltAndroidTest
class ToDoDaoTest {

    val exceptions = mutableListOf<Throwable>()
    val customCaptor = CoroutineExceptionHandler { ctx, throwable ->
        exceptions.add(throwable) // add proper synchronization if the test is multithreaded
    }

    @get:Rule
    var instantTaskExecuterRule = InstantTaskExecutorRule()

    @Inject
    @Named("DataBaseTest")
    lateinit var dataBase: ToDoDataBase

    private lateinit var dao: ToDoDao

    @get:Rule
    var hiltRule = HiltAndroidRule(this)


    @Before
    fun setup() {
        hiltRule.inject()
        dao = dataBase.toDoDao()
    }

    @After
    fun tearDown() {
        dataBase.close()
    }

    @Test
    fun insertToDoTask() = runTest {
        launch(customCaptor) {

            val todo = ToDoTask(1, "title", "desc", Priority.HIGHT)
            dao.addTask(todo)


            dao.getAllTask().test {
                val list = awaitItem()
                assert(list.contains(todo))
                cancel()
            }
        }
    }

    @Test
    fun deleteToDoTask() = runTest {
        launch(customCaptor) {

            val todo = ToDoTask(1, "title", "desc", Priority.HIGHT)
            dao.addTask(todo)
            dao.deleteTask(todo)


            dao.getAllTask().test {
                val list = awaitItem()
                assert(!list.contains(todo))
                cancel()

            }
        }
    }
}