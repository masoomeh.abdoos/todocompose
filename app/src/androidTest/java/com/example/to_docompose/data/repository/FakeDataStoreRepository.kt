package com.example.to_docompose.data.repository

import com.example.to_docompose.data.model.Priority
import kotlinx.coroutines.flow.Flow

public open class FakeDataStoreRepository :DataStoreRepository {
    override suspend fun persistSortState(priority: Priority) {

    }

    override var readSortState: Flow<String>
        get() = TODO("Not yet implemented")
        set(value) {}
}