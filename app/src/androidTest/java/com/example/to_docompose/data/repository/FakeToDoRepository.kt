package com.example.to_docompose.data.repository

import com.example.to_docompose.data.model.Priority
import com.example.to_docompose.data.model.ToDoTask
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.flow

 open class FakeToDoRepository : ToDoRepository {

    private val tasks = mutableListOf<ToDoTask>()
    private val _tasksFlow = MutableStateFlow<List<ToDoTask>>(emptyList())


    override var getAllTasks: Flow<List<ToDoTask>> = flow {
        tasks.add(0,ToDoTask(
            1,"test","desc",Priority.HIGHT
        ))
        tasks?.let { emit(it) } }

    override fun getSelectedTask(taskId: Int): Flow<ToDoTask> {
         var task = tasks.find { it.id == taskId }
        return flow {
            task?.let { emit(it) }
        }
    }

    override fun searchDataBase(searchQuery: String): Flow<List<ToDoTask>> {
        var tasksResult = tasks.filter {
            it.title.contains(searchQuery) || it.description.contains(searchQuery)
        }

        return flow {
            emit(tasksResult)
        }
    }

    override suspend fun addTask(toDoTask: ToDoTask) {
        tasks.add(toDoTask)
        _tasksFlow.value = tasks
    }

    override suspend fun updateTask(toDoTask: ToDoTask) {
        var index = tasks.indexOfFirst { it.id == toDoTask.id }
        if(index != -1){
            tasks[index] = toDoTask
        }
        _tasksFlow.value = tasks
    }

    override suspend fun deleteTask(toDoTask: ToDoTask) {
        tasks.remove(toDoTask)
        _tasksFlow.value = tasks
    }

    override suspend fun deleteAllTask() {
        tasks.clear()
        _tasksFlow.value = tasks
    }
}