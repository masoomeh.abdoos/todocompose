package com.example.to_docompose.ui.viewmodel

import android.content.Context
import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.test.ext.junit.runners.AndroidJUnit4
import app.cash.turbine.test
import com.example.to_docompose.data.repository.DataStoreRepository
import com.example.to_docompose.data.repository.ToDoRepository
import com.example.to_docompose.ui.viwemodel.ShareViewModel
import com.example.to_docompose.util.RequestState
import dagger.hilt.android.testing.HiltAndroidRule
import dagger.hilt.android.testing.HiltAndroidTest
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.resetMain
import kotlinx.coroutines.test.runTest
import kotlinx.coroutines.test.setMain
import org.junit.After
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.MockitoAnnotations
import javax.inject.Inject
import javax.inject.Named


@ExperimentalCoroutinesApi
@HiltAndroidTest
@RunWith(AndroidJUnit4::class)
class ShareViewModelTest {

    private lateinit var shareViewModel: ShareViewModel

    @get:Rule
    var instantTaskExecutorRule = InstantTaskExecutorRule()


    @Inject
    lateinit var context: Context

    @get:Rule
    var hiltRule = HiltAndroidRule(this)

    @Inject
    @Named("fakeToDoRepo")
    lateinit  var fakeToDoRepository :ToDoRepository

    @Inject
    lateinit  var fakeDataStoreRepository :DataStoreRepository
    @Before
    fun step(){

        hiltRule.inject()
        MockitoAnnotations.initMocks(this)

        Dispatchers.setMain(Dispatchers.Unconfined)

        shareViewModel = ShareViewModel(
            toDoRepository = fakeToDoRepository,
            dataStoreRepository = fakeDataStoreRepository,
            context = context
        )
    }

    @After
    fun tearDown() {
        Dispatchers.resetMain()
    }

    @Test
    fun get_all_task_list_with_error() = runTest {

        val result = shareViewModel.allTask
        assert(result.value is RequestState.Idle)

        shareViewModel.getAllTasks()

        shareViewModel.allTask.test {
            val list = awaitItem()

            assert(list != RequestState.Error(Throwable()))
            assert(list is RequestState.Success)
            cancel()

        }


    }
}