package com.example.to_docompose.data.repository

import android.content.Context
import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import androidx.datastore.preferences.core.emptyPreferences
import androidx.datastore.preferences.core.stringPreferencesKey
import androidx.datastore.preferences.preferencesDataStore
import com.example.to_docompose.data.model.Priority
import com.example.to_docompose.util.Constants
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.map
import java.io.IOException
import javax.inject.Inject


val Context.dataStore : DataStore<Preferences> by preferencesDataStore(name = Constants.PREFERENCE_NAME)

class DataStoreRepositoryImpl @Inject constructor(@ApplicationContext val context: Context)
    :DataStoreRepository{

    object PreferenceKeys {
        val sortState = stringPreferencesKey(name = Constants.PREFERENCE_KEY)
    }

    private val dataStore = context.dataStore

    override suspend fun persistSortState(priority: Priority){
        dataStore.edit { preferences ->
            preferences[PreferenceKeys.sortState] = priority.name
        }
    }

    override var readSortState: Flow<String> = dataStore.data
        .catch {exception ->
            if(exception is IOException){
                emit(emptyPreferences())
            }else{
                throw exception
            }

        }
        .map {pref ->
            pref[PreferenceKeys.sortState]?:Priority.NONE.name
        }
}