package com.example.to_docompose.data

import androidx.room.*
import com.example.to_docompose.data.model.Priority
import com.example.to_docompose.data.model.ToDoTask
import kotlinx.coroutines.flow.Flow

@Dao
interface ToDoDao {

    @Query("select * from todo_table order by id asc")
    fun getAllTask(): Flow<List<ToDoTask>>

//    @Query("select * from todo_table where priority =:priority  order by id asc ")
//    fun getAllTask(priority: Priority =Priority.MEDUIM): Flow<List<ToDoTask>>

    @Query("select * from todo_table  where id =:taskId")
    fun getSelectedTask(taskId : Int): Flow<ToDoTask>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addTask(toDoTask: ToDoTask):Long /*Flow<ToDoTask>*/

    @Update
    suspend fun updateTask(toDoTask: ToDoTask)

    @Delete
    suspend fun deleteTask(toDoTask: ToDoTask)

    @Query("delete from todo_table")
    suspend fun deleteAllTasks()


    @Query("select * from todo_table where title like :searchQuery  or description like :searchQuery ")
    fun searchTask(searchQuery:String) : Flow<List<ToDoTask>>

}