package com.example.to_docompose.data.repository

import com.example.to_docompose.data.model.ToDoTask
import kotlinx.coroutines.flow.Flow

interface ToDoRepository {

    var getAllTasks : Flow<List<ToDoTask>>
    fun getSelectedTask(taskId:Int) : Flow<ToDoTask>
    fun searchDataBase(searchQuery:String) : Flow<List<ToDoTask>>
    suspend fun addTask(toDoTask: ToDoTask)
    suspend fun updateTask(toDoTask: ToDoTask)
    suspend fun deleteTask(toDoTask: ToDoTask)
    suspend fun deleteAllTask()
}