package com.example.to_docompose.data.repository

import com.example.to_docompose.data.model.Priority
import kotlinx.coroutines.flow.Flow

interface DataStoreRepository {
    suspend fun persistSortState(priority: Priority)
   var  readSortState : Flow<String>
}