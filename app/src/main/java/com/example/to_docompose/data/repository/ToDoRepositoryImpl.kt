package com.example.to_docompose.data.repository

import com.example.to_docompose.data.ToDoDao
import com.example.to_docompose.data.model.ToDoTask
import dagger.hilt.android.scopes.ViewModelScoped
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class ToDoRepositoryImpl @Inject constructor(private val toDoDao: ToDoDao)
    :ToDoRepository{

    override var getAllTasks = toDoDao.getAllTask()

    override fun getSelectedTask(taskId:Int) : Flow<ToDoTask> {
        return toDoDao.getSelectedTask(taskId)
    }

    override fun searchDataBase(searchQuery:String) : Flow<List<ToDoTask>> {
        return toDoDao.searchTask(searchQuery)
    }

    override suspend fun addTask(toDoTask: ToDoTask){
        toDoDao.addTask(toDoTask)
    }

    override suspend fun updateTask(toDoTask: ToDoTask){
        toDoDao.updateTask(toDoTask)
    }

    override suspend fun deleteTask(toDoTask: ToDoTask){
        toDoDao.deleteTask(toDoTask)
    }

    override suspend fun deleteAllTask(){
        toDoDao.deleteAllTasks()
    }
}