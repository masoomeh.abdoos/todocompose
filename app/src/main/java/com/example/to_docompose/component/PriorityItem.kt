package com.example.to_docompose.component

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.layout.size
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.to_docompose.data.model.Priority
import com.example.to_docompose.ui.theme.MEDIUM_PADDING
import com.example.to_docompose.ui.theme.PRIORITY_INDICATER_SIZE


@Composable
fun PriorityItem(priority: Priority){

    Row (
        verticalAlignment = Alignment.CenterVertically
        ) {
            Canvas(modifier = Modifier.size(PRIORITY_INDICATER_SIZE)){
                drawCircle(color = priority.color )
            }
        Text(
            text = priority.name,
            modifier = Modifier.padding(start = MEDIUM_PADDING)
            )

    }

}


@Preview
@Composable
fun PriorityItemPreview(){

    PriorityItem(priority = Priority.HIGHT)

}