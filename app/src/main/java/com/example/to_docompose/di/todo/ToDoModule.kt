package com.example.to_docompose.di.todo

import com.example.to_docompose.data.repository.ToDoRepository
import com.example.to_docompose.data.repository.ToDoRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class ToDoModule {

    @Binds
    @Singleton
    abstract fun provideRepository(impl: ToDoRepositoryImpl): ToDoRepository
}