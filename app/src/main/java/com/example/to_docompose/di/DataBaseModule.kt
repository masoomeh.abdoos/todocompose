package com.example.to_docompose.di

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import com.example.to_docompose.data.ToDoDataBase
import com.example.to_docompose.util.Constants.DATABASE_NAME
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class DataBaseModule {

    @Provides
    @Singleton
    fun provideDataBase(@ApplicationContext context: Context)
    = Room.databaseBuilder(context,ToDoDataBase::class.java,DATABASE_NAME).build()

    @Provides
    @Singleton
    fun provideDao(database: ToDoDataBase) = database.toDoDao()
}