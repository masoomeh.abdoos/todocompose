package com.example.to_docompose.di.datastore

import com.example.to_docompose.data.repository.DataStoreRepository
import com.example.to_docompose.data.repository.DataStoreRepositoryImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
abstract class DataStoreModule {

    @Binds
    @Singleton
    abstract fun provideRepository(impl: DataStoreRepositoryImpl): DataStoreRepository
}