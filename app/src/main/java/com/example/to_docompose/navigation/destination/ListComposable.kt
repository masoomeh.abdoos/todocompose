package com.example.to_docompose.navigation.destination

import android.util.Log
import androidx.compose.runtime.LaunchedEffect
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.to_docompose.ui.screen.list.ListScreen
import com.example.to_docompose.ui.viwemodel.ShareViewModel
import com.example.to_docompose.util.Constants.LIST_ARGUMENT_KEY
import com.example.to_docompose.util.Constants.LIST_SCREEN
import com.example.to_docompose.util.toAction


fun NavGraphBuilder.listComposable(
    navigationToTaskScreen: (Int) -> Unit,
    shareViewModel: ShareViewModel
) {

    composable(route =LIST_SCREEN ,
        arguments = listOf(navArgument( LIST_ARGUMENT_KEY){
            type = NavType.StringType
        })
    ){ navBackStackEntry ->
        val action = navBackStackEntry?.arguments?.getString(LIST_ARGUMENT_KEY).toAction()

        LaunchedEffect(key1 = action ){
            shareViewModel.action.value  = action
        }
        ListScreen(navigationToTaskScreen = navigationToTaskScreen,shareViewModel)
//        shareViewModel.getAllTasks("11111")
    }
}