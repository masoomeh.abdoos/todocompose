package com.example.to_docompose.navigation.destination

import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.navigation.NavGraphBuilder
import androidx.navigation.NavType
import androidx.navigation.compose.composable
import androidx.navigation.navArgument
import com.example.to_docompose.ui.screen.task.TaskScreen
import com.example.to_docompose.ui.viwemodel.ShareViewModel
import com.example.to_docompose.util.Action
import com.example.to_docompose.util.Constants

fun NavGraphBuilder.taskComposable(
    shareViewModel: ShareViewModel,
    navigationToListScreen :(Action) -> Unit
) {

    composable(
        route = Constants.TASK_SCREEN,
        arguments = listOf(navArgument( Constants.TASK_ARGUMENT_KEY){
            type = NavType.IntType
        })
    ){ navBackStackEntry ->
        var taskId = navBackStackEntry.arguments!!.getInt(Constants.TASK_ARGUMENT_KEY)

        shareViewModel.getSelecteTask(taskId = taskId)
        val selectedTask by shareViewModel.selectedTask.collectAsState()

        LaunchedEffect(key1 = selectedTask?.id){

            if (selectedTask?.id != null ||taskId ==-1) {
                shareViewModel.updateTaskField(selectTask = selectedTask)
                shareViewModel.validationTitle()
                shareViewModel.validationDesc()
            }
        }

        TaskScreen(
            navigationToListScreen = navigationToListScreen,
            shareViewModel = shareViewModel,
            selectedTask = selectedTask)
    }
}