package com.example.to_docompose.navigation

import androidx.compose.runtime.Composable
import androidx.compose.runtime.remember
import androidx.navigation.NavHostController
import androidx.navigation.compose.NavHost
import com.example.to_docompose.navigation.destination.listComposable
import com.example.to_docompose.navigation.destination.taskComposable
import com.example.to_docompose.ui.viwemodel.ShareViewModel
import com.example.to_docompose.util.Constants.LIST_SCREEN

@Composable
fun SetupNavigation(navHostController: NavHostController, shareViewModel: ShareViewModel) {

    val screens = remember(navHostController) {
        Screens(navHostController)
    }


    // composable or screens
    NavHost(navHostController , LIST_SCREEN){

        listComposable(
            navigationToTaskScreen = screens.task ,
            shareViewModel = shareViewModel)

        taskComposable (
            navigationToListScreen = screens.list,
            shareViewModel = shareViewModel)
    }
}