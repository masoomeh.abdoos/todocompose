package com.example.to_docompose.navigation

import androidx.navigation.NavHostController
import com.example.to_docompose.util.Action
import com.example.to_docompose.util.Constants.LIST_SCREEN
import com.example.to_docompose.util.Constants.TASK_SCREEN

class Screens(navHostController: NavHostController) {

    //have 2 variable

    val list : (Action) ->Unit = { action ->
        navHostController.navigate("list/${action}"){
            popUpTo(LIST_SCREEN){
                inclusive = true
            }
       }
    }



    val task : (Int) ->Unit = { taskId ->
        navHostController.navigate("task/${taskId}")
    }
}