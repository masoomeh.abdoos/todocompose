package com.example.to_docompose.ui.screen.list

import androidx.compose.foundation.layout.fillMaxHeight
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.height
import androidx.compose.foundation.layout.padding
import androidx.compose.foundation.text.KeyboardActionScope
import androidx.compose.foundation.text.KeyboardActions
import androidx.compose.foundation.text.KeyboardOptions
import androidx.compose.material.*
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Close
import androidx.compose.material.icons.filled.Search
import androidx.compose.runtime.*
import androidx.compose.ui.res.painterResource
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import com.example.to_docompose.ui.theme.fabBackgroundColor
import com.example.to_docompose.R
import androidx.compose.ui.Modifier
import androidx.compose.ui.draw.alpha
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.text.TextStyle
import androidx.compose.ui.text.input.ImeAction
import com.example.to_docompose.component.PriorityItem
import com.example.to_docompose.data.model.Priority
import com.example.to_docompose.ui.theme.SMALL_PADDING
import com.example.to_docompose.ui.theme.TOP_APP_BAR_HEIGHT
import com.example.to_docompose.ui.theme.topAppbarColor
import com.example.to_docompose.ui.viwemodel.ShareViewModel
import com.example.to_docompose.util.SearchAppBarState
import com.example.to_docompose.util.TrailingIconState


@Composable
fun ListAppBar(
    shareViewModel: ShareViewModel,
    searchAppBarState : SearchAppBarState,
    searchTextState : String
){
    when(searchAppBarState){

        SearchAppBarState.CLOSED ->
            DefaultListAppBar(
                    onSearchClick = {
                                    shareViewModel.searchAppBarState.value =
                                        SearchAppBarState.OPENED
                    },
                    onSortClick = {
                                  shareViewModel.persistSortData(it)
                    },
                    onDeleteClick = {}
            )


        else ->
            SearchAppBar(
                text = searchTextState,
                onTextChange = {
                               shareViewModel.searchTextState.value = it
                },
                onSearchClick = {
                                shareViewModel.searchDataBase(searchQuery = it )
                },
                onCloseClick = {
                    shareViewModel.searchAppBarState.value =
                        SearchAppBarState.CLOSED
                    shareViewModel.searchTextState.value = ""
                }
            )

    }


}

@Composable
fun DefaultListAppBar(onSearchClick:()-> Unit,
                      onSortClick:(Priority)-> Unit,
                      onDeleteClick: () -> Unit
                      ){
    TopAppBar (
        title = {
            Text(text = "Sample")
        }, backgroundColor = MaterialTheme.colors.primary
    , actions = {ListAppBarActions (onSearchClick,onSortClick, onDeleteClick =onDeleteClick )}
    )
}

@Composable
fun ListAppBarActions(
    onSearchClick:()-> Unit,
    onSortClick:(Priority)-> Unit,
    onDeleteClick:()-> Unit,

    ){
    SearchAction(onSearchClick)
    SortAction (onSortClick)
    DeleteAction(onDeleteClick)
}

@Composable
fun SearchAction(onSearchClick:()-> Unit) {

    IconButton(onClick = onSearchClick) {
        Icon(
            imageVector = Icons.Filled.Search,
            contentDescription = stringResource(id = R.string.search_button),
            tint = MaterialTheme.colors.fabBackgroundColor
        )

    }
}


@Composable
fun SortAction(onSortClick: (Priority) -> Unit) {

    var expanded by remember { mutableStateOf(false) }

    IconButton(onClick = {expanded = true}) {

        Icon(
            painter = painterResource(id = R.drawable.ic_filter_action),
            contentDescription = stringResource(
                id = R.string.filter_button
            )
        )

        DropdownMenu(
            expanded = false,
            onDismissRequest = { expanded = false })
        {
            DropdownMenuItem(
                onClick = {
                    expanded =false
                    onSortClick(Priority.HIGHT)
                }
            ) {
                PriorityItem(priority = Priority.HIGHT)
            }

            DropdownMenuItem(
                onClick = {
                    expanded =false
                    onSortClick(Priority.MEDUIM)
                }
            ) {
                PriorityItem(priority = Priority.MEDUIM)
            }

            DropdownMenuItem(
                onClick = {
                    expanded =false
                    onSortClick(Priority.LOW)
                }
            ) {
                PriorityItem(priority = Priority.LOW)
            }

            DropdownMenuItem(
                onClick = {
                    expanded =false
                    onSortClick(Priority.HIGHT)
                }
            ) {
                PriorityItem(priority = Priority.NONE)
            }
        }
    }
}

@Composable
fun  DeleteAction(onDeleteClick: () -> Unit){
    var expanded by remember { mutableStateOf(false) }

    IconButton(onClick = {expanded = true}) {

        Icon(
            painter = painterResource(id = R.drawable.ic_vector_more),
            contentDescription = stringResource(
                id = R.string.filter_button
            )
        )

        DropdownMenu(
            expanded = false,
            onDismissRequest = { expanded = false })
        {
            DropdownMenuItem(
                onClick = {
                    expanded = false
                    onDeleteClick()
                }
            ) {
                Text(modifier = Modifier.padding( SMALL_PADDING),
                    text = stringResource(id = R.string.delete_all))
            }
        }
    }
}

@Composable
fun SearchAppBar(
    text:String,
    onTextChange:(String)->Unit,
    onSearchClick: (String) -> Unit,
    onCloseClick: () -> Unit
){

    var trailingIconState by remember {
        mutableStateOf(TrailingIconState.READY_TO_DELETE)
    }
    Surface(modifier = Modifier
        .fillMaxWidth()
        .height(TOP_APP_BAR_HEIGHT),
    elevation = AppBarDefaults.TopAppBarElevation,
    color = MaterialTheme.colors.topAppbarColor ) {


        TextField(modifier = Modifier.fillMaxWidth(),
            value = text,
            onValueChange = {
                onTextChange(it)
            },

            //placeholder
            placeholder = {
                Text(
                    modifier = Modifier.alpha(ContentAlpha.medium),
                    text = "search",
                    color = Color.White
                )
            } ,
            textStyle = TextStyle(
                color = Color.White,
                fontSize = MaterialTheme.typography.subtitle1.fontSize,
            ),
            singleLine = true,
            //leading icon
            leadingIcon = {
                IconButton(onClick ={}) {

                    Icon(
                        imageVector = Icons.Filled.Search,
                        contentDescription = stringResource(id = R.string.search_button),
                        tint = MaterialTheme.colors.topAppbarColor
                    )
                    
                }
            },
            trailingIcon = {
                IconButton(
                    onClick = {
                       when (trailingIconState){
                           TrailingIconState.READY_TO_DELETE -> {
                               trailingIconState = TrailingIconState.READY_TO_CLOSE
                               onTextChange("")
                           }
                           else ->{
                               if(text.isNotEmpty()){
                                   onTextChange("")
                               }else{
                                   trailingIconState = TrailingIconState.READY_TO_DELETE
                               }
                           }
                       }
                    }
                ) {
                    Icon(
                        imageVector = Icons.Filled.Close,
                        contentDescription = "",
                    tint = MaterialTheme.colors.topAppbarColor)

                }
            },
            keyboardOptions = KeyboardOptions(
                imeAction = ImeAction.Search
            ),
            keyboardActions = KeyboardActions(
                onSearch={
                    onSearchClick(text)
                }
            ),
            colors = TextFieldDefaults.textFieldColors(
                //set cursor , foucusIndicate, unfocuseIndicate,backgroundColor
            )

        )
        
    }

}


@Preview
@Composable
fun DefaultListAppBarPreview() {
    DefaultListAppBar(
        onSearchClick = {},
        onSortClick = {},
        onDeleteClick = {}
    )
}

@Composable
@Preview
fun AppBarSearch() {
    SearchAppBar(
        text = "",
        onTextChange = {},
        onSearchClick = {},
        onCloseClick = {})
}

