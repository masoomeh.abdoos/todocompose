package com.example.to_docompose.ui.screen.task

import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.Scaffold
import androidx.compose.runtime.Composable
import androidx.compose.runtime.getValue
import androidx.compose.ui.Modifier
import com.example.to_docompose.data.model.ToDoTask
import com.example.to_docompose.ui.viwemodel.ShareViewModel
import com.example.to_docompose.util.Action

@Composable
fun TaskScreen(
    navigationToListScreen :(Action)->Unit,
    shareViewModel: ShareViewModel,
    selectedTask: ToDoTask?
){
    val title by shareViewModel.title
    val isValidTitle by shareViewModel.isTitleValid
    val errorTitleValidation by shareViewModel.titleErrMsg
    val description by shareViewModel.description
    val isDesValid by shareViewModel.isDescriptionValid
    val errorDesValidation by shareViewModel.descriptionErrMsg
    val priority by shareViewModel.priority
    Scaffold(
        topBar = {
            TaskActionBar(
                selectedTask = selectedTask,
                navigationToListScreen = {action ->
                    if(action ==Action.NO_ACTION){
                        navigationToListScreen(action)
                    }else{
                        if(shareViewModel.validationTitle()
                            && shareViewModel.validationDesc())
                            navigationToListScreen(action)
                    }
                }
            )
                 },
        content = {
            Column(
                modifier = Modifier
                    .padding(it)
            ) {
            TaskContent(
                title = title,
                onTitleChange = {
                    shareViewModel.title.value = it
                },
                isTitleValid = isValidTitle,
                errorTextTitle = errorTitleValidation,
                description = description,
                onDescriptionChange = {desc ->
                    shareViewModel.description.value = desc
                },
                isDescriptionValid  = isDesValid,
                errorDescriptionTitle = errorDesValidation,
                priority = priority
            ) {
                shareViewModel.priority.value = it
            }
            }
        }
    )
}
