package com.example.to_docompose.ui.screen.list

import android.util.Log
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.padding
import androidx.compose.material.FloatingActionButton
import androidx.compose.material.Icon
import androidx.compose.material.Scaffold
import androidx.compose.material.ScaffoldState
import androidx.compose.material.SnackbarResult
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.Add
import androidx.compose.material.rememberScaffoldState
import androidx.compose.runtime.Composable
import androidx.compose.runtime.LaunchedEffect
import androidx.compose.runtime.collectAsState
import androidx.compose.runtime.getValue
import androidx.compose.runtime.rememberCoroutineScope
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import com.example.to_docompose.R
import com.example.to_docompose.ui.viwemodel.ShareViewModel
import com.example.to_docompose.util.Action
import com.example.to_docompose.util.SearchAppBarState
import kotlinx.coroutines.launch

@Composable
fun ListScreen(
    navigationToTaskScreen:(Int) ->Unit,
    shareViewModel: ShareViewModel
    ) {


    LaunchedEffect(key1 = true) {
        shareViewModel.getAllTasks()
    }

    val action by shareViewModel.action

    val allTask by shareViewModel.allTask.collectAsState()
    val searchedTasks by shareViewModel.searchedTask.collectAsState()

    val searchAppBarState: SearchAppBarState by shareViewModel.searchAppBarState
    val searchTextState: String by shareViewModel.searchTextState
    val scaffoldState = rememberScaffoldState()


    DisplaySnackBar(
        scaffoldState = scaffoldState,
        taskTitle = shareViewModel.title.value ,
        action = action,
        undoClickDelete = {
            shareViewModel.action.value = it
        }
    )

    shareViewModel.handleDataBaseAction(action = action)

    Scaffold(
        scaffoldState = scaffoldState,
        topBar = {
            ListAppBar(
                shareViewModel = shareViewModel,
                searchAppBarState = searchAppBarState,
                searchTextState = searchTextState
            )
        },
        content = {

            Column(modifier = Modifier.padding(it)) {

                ListContent(
                    allTasks = allTask,
                    searchedTasks = searchedTasks,
                    searchedAppBarState = searchAppBarState,
                    navigationToTaskScreen = navigationToTaskScreen
                )
            }
        },
        floatingActionButton = {
            ListFab(navigationToTaskScreen = navigationToTaskScreen)
        })
}

@Composable
fun ListFab(navigationToTaskScreen: (Int) -> Unit){
    FloatingActionButton(
        onClick = {
        navigationToTaskScreen(-1)
        }
    ){
        Icon(imageVector = Icons.Filled.Add,
            contentDescription = stringResource(id = R.string.app_name),
            tint = Color.White)
    }


}


@Composable
fun DisplaySnackBar(
    scaffoldState: ScaffoldState,
    taskTitle:String,
    action:Action,
    undoClickDelete:(Action)->Unit
) {
    val scope = rememberCoroutineScope()

    Log.w("TAG", "DisplaySnackBar: ${action.name}" )
    LaunchedEffect(key1 =action) {
        if (action.name != Action.NO_ACTION.name) {
            scope.launch {
                val snackBarResult = scaffoldState.snackbarHostState.showSnackbar(
                    message = "${action.name}: $taskTitle",
                    actionLabel = setActionLabel(action = action)
                )

                undoDeleteTask(snackBarResult = snackBarResult,
                    action =action,
                    undoClickDelete = undoClickDelete
                    )
            }
        }
    }
}


fun setActionLabel(action :Action):String{
   return if(action ==Action.DELETE){
       "Undo"
    }else{
        "Ok"
    }
}

fun undoDeleteTask(
    snackBarResult: SnackbarResult,
    action :Action,
    undoClickDelete:(Action)->Unit
){
    if(action ==Action.DELETE &&
        snackBarResult ==SnackbarResult.ActionPerformed){
        undoClickDelete(Action.UNDO)
    }
}

//@Preview
//@Composable
//fun ListScreenPreview(){
//    ListScreen({})
//}