package com.example.to_docompose.ui.viwemodel

import android.content.Context
import android.util.Log
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.to_docompose.R
import com.example.to_docompose.data.model.Priority
import com.example.to_docompose.data.model.ToDoTask
import com.example.to_docompose.data.repository.DataStoreRepository
import com.example.to_docompose.data.repository.DataStoreRepositoryImpl
import com.example.to_docompose.data.repository.ToDoRepository
import com.example.to_docompose.data.repository.ToDoRepositoryImpl
import com.example.to_docompose.util.Action
import com.example.to_docompose.util.Constants.NAX_TITLE_LENGTH
import com.example.to_docompose.util.RequestState
import com.example.to_docompose.util.SearchAppBarState
import dagger.hilt.android.lifecycle.HiltViewModel
import dagger.hilt.android.qualifiers.ApplicationContext
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.map
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ShareViewModel @Inject constructor
    (
    private val toDoRepository: ToDoRepository,
    private val dataStoreRepository: DataStoreRepository,
    @ApplicationContext private val context: Context
    ) : ViewModel() {

    val action :MutableState<Action> = mutableStateOf(Action.NO_ACTION)

    val id :MutableState<Int> = mutableStateOf(0)
    val title :MutableState<String> = mutableStateOf("")
    var isTitleValid: MutableState<Boolean> = mutableStateOf(true)
    var titleErrMsg: MutableState<String> = mutableStateOf("")

    val description :MutableState<String> = mutableStateOf("")
    var isDescriptionValid: MutableState<Boolean> = mutableStateOf(true)
    var descriptionErrMsg: MutableState<String> = mutableStateOf("")

    val priority :MutableState<Priority> = mutableStateOf(Priority.LOW)

    private var _allTasks =
        MutableStateFlow<RequestState<List<ToDoTask>>>(RequestState.Idle)
    val allTask : StateFlow<RequestState<List<ToDoTask>>> = _allTasks

    val searchAppBarState : MutableState<SearchAppBarState> =
        mutableStateOf(SearchAppBarState.CLOSED)
    val searchTextState : MutableState<String> =
        mutableStateOf("")

    fun getAllTasks(){
        _allTasks.value = RequestState.Loading
        try {
            viewModelScope.launch {
                toDoRepository.getAllTasks.collect {
                    _allTasks.value = RequestState.Success(it)
                }
            }
        }catch (e:Throwable){
            _allTasks.value = RequestState.Error(e)
        }
        searchAppBarState.value = SearchAppBarState.CLOSED
    }


    private var _searchedTask =
        MutableStateFlow<RequestState<List<ToDoTask>>>(RequestState.Idle)

    val searchedTask : StateFlow<RequestState<List<ToDoTask>>> = _searchedTask

    fun searchDataBase(searchQuery:String){
        _searchedTask.value = RequestState.Loading
        try {
            viewModelScope.launch {
                toDoRepository.searchDataBase(searchQuery = "%$searchQuery%").collect {
                    _searchedTask.value = RequestState.Success(it)
                }
            }
        }catch (e:Throwable){
            _searchedTask.value = RequestState.Error(e)
        }
        searchAppBarState.value = SearchAppBarState.TRIGGERED
    }


    val _selectedTask : MutableStateFlow<ToDoTask?> = MutableStateFlow(null)
    val selectedTask : StateFlow<ToDoTask?> = _selectedTask

    fun getSelecteTask (taskId:Int){
//        Log.d("TAG", "getSelecteTask: $taskId")
        viewModelScope.launch {
            toDoRepository.getSelectedTask(taskId = taskId).collect(){ task ->
                _selectedTask.value = task
            }
        }
    }

    fun addTask(){
        viewModelScope.launch (Dispatchers.IO) {
            val toDoDao = ToDoTask(
                title = title.value,
                description = description.value,
                priority = priority.value
            )
            toDoRepository.addTask(toDoTask = toDoDao)
        }
    }

    fun deleteTask(){
        viewModelScope.launch (Dispatchers.IO) {
            val toDoDao = ToDoTask(
                id = id.value,
                title = title.value,
                description = description.value,
                priority = priority.value
            )
            toDoRepository.deleteTask(toDoTask = toDoDao)
        }
    }


    fun updateTask(){

        viewModelScope.launch (Dispatchers.IO) {
            val toDoDao = ToDoTask(
                id = id.value,
                title = title.value,
                description = description.value,
                priority = priority.value
            )
            toDoRepository.updateTask(toDoTask = toDoDao)
        }
    }

    fun  handleDataBaseAction(action: Action){
        Log.d("TAG", "handleDataBaseAction() called with: action = ${action.name}")
        when(action)
        {
            Action.ADD ->{
                addTask()
            }

            Action.UPDATE ->{
                updateTask()
            }

            Action.DELETE ->{
                deleteTask()
            }

            Action.UNDO ->{
                addTask()
            }

            else -> {

            }
        }
        this.action.value = Action.NO_ACTION

    }
    fun updateTaskField (selectTask:ToDoTask?){

        if(selectTask != null){
            id.value = selectTask.id
            title.value = selectTask.title
            description.value = selectTask.description
            priority.value = selectTask.priority
        }else{
            id.value = 0
            title.value = ""
            description.value = ""
            priority.value = Priority.LOW
        }
    }


     fun persistSortData(priority: Priority){
         viewModelScope.launch (Dispatchers.IO){
             dataStoreRepository.persistSortState(priority = priority)
         }
     }

    private var _sortState =
        MutableStateFlow<RequestState<Priority>>(RequestState.Idle)

    val sortState : StateFlow<RequestState<Priority>> = _sortState

    fun readSortState(){
        _sortState.value = RequestState.Loading
        try {
            viewModelScope.launch {
                dataStoreRepository.readSortState
                    .map { Priority.valueOf(it) }
                    .collect{
                        _sortState.value = RequestState.Success(it)
                    }
            }
        }catch (e:Throwable){
            _sortState.value = RequestState.Error(e)
        }
    }


//    fun updateTitle(newTitle: String){
//        if(newTitle.length < NAX_TITLE_LENGTH){
//            title.value = newTitle
//        }
//    }


    fun validationTitle() :Boolean{
        if(title.value.length  <=0 ){
            isTitleValid.value = false
            titleErrMsg.value = context.getString( R.string.empty_value)
        }else if(title.value.length  > NAX_TITLE_LENGTH ) {
            isTitleValid.value = false
            titleErrMsg.value = context.getString( R.string.the_charachter_is_big)
        } else{
            isTitleValid.value = true
            titleErrMsg.value = ""
        }
        return isTitleValid.value
    }


    fun validationDesc() :Boolean{
        if(description.value.length <= 0) {
            isDescriptionValid.value = false
            descriptionErrMsg.value = context.getString( R.string.empty_value)

        }else{
            isDescriptionValid.value = true
            descriptionErrMsg.value = ""
        }

        return isDescriptionValid.value
    }
}