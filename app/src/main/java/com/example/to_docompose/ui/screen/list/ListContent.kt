package com.example.to_docompose.ui.screen.list

import androidx.compose.foundation.Canvas
import androidx.compose.foundation.Image
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material.ExperimentalMaterialApi
import androidx.compose.material.MaterialTheme
import androidx.compose.material.Surface
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.RectangleShape
import androidx.compose.ui.text.font.FontWeight
import androidx.compose.ui.tooling.preview.Preview
import com.example.to_docompose.data.model.Priority
import com.example.to_docompose.data.model.ToDoTask
import com.example.to_docompose.ui.theme.PRIORITY_INDICATER_SIZE
import com.example.to_docompose.ui.theme.SMALL_PADDING
import com.example.to_docompose.ui.theme.TASK_ITEM_ELEVATION
import com.example.to_docompose.util.RequestState
import com.example.to_docompose.util.SearchAppBarState

@Composable
fun ListContent(
    allTasks: RequestState<List<ToDoTask>>,
    searchedTasks: RequestState<List<ToDoTask>>,
    searchedAppBarState: SearchAppBarState,
    navigationToTaskScreen: (taskId: Int) -> Unit
) {
    if(searchedAppBarState == SearchAppBarState.TRIGGERED){
        if(searchedTasks is  RequestState.Success){
            HandleListContent(
                tasks = searchedTasks.data,
                navigationToTaskScreen = navigationToTaskScreen
             )
        }

    }else{
        if(allTasks is  RequestState.Success){
            HandleListContent(
                tasks = allTasks.data,
                navigationToTaskScreen = navigationToTaskScreen
            )

        }

    }


}
@Composable
fun HandleListContent(
    tasks :List<ToDoTask>,
    navigationToTaskScreen: (taskId: Int) -> Unit
){
        if (tasks.isEmpty()) {
            EmptyContent()
        } else {
            DisplayTasks(
                tasks = tasks,
                navigationToTaskScreen =navigationToTaskScreen )

        }
}
@Composable
fun DisplayTasks(
    tasks: List<ToDoTask>,
    navigationToTaskScreen: (taskId: Int) -> Unit){
    LazyColumn {

        items(
            items = tasks,
            key = { task ->
                task.id
            }){ task ->
            TaskItem(
                toDoTask = task,
                navigationToTaskScreen=navigationToTaskScreen
            )
        }
    }
}

@OptIn(ExperimentalMaterialApi::class)
@Composable
fun TaskItem(toDoTask: ToDoTask,
             navigationToTaskScreen: (taskId: Int) -> Unit){
    Surface(modifier = Modifier.
            fillMaxWidth(),
            shape = RectangleShape,
            elevation = TASK_ITEM_ELEVATION,
            onClick = {
                navigationToTaskScreen(toDoTask.id)
            }
    ) {
        Column(modifier = Modifier.padding( SMALL_PADDING)) {
            Row {
                Text(modifier = Modifier.weight(8f),
                     text = toDoTask.title,
                    fontWeight = FontWeight.Bold,
                    maxLines = 1,
                    style = MaterialTheme.typography.h5
                     )

                Box(modifier = Modifier
                    .fillMaxWidth()
                    .weight(1f),
                    contentAlignment = Alignment.TopEnd,
                    ){
                    Canvas(modifier = Modifier
                        .height(PRIORITY_INDICATER_SIZE)
                        .width(PRIORITY_INDICATER_SIZE) ){
                        drawCircle(color = toDoTask.priority.color)
                    }
                }
            }
            Text(modifier = Modifier.fillMaxWidth(),
                 text = toDoTask.description,
                 style = MaterialTheme.typography.subtitle2,
                 maxLines = 2
            )
        }
    }
}
@Preview
@Composable
fun TaskItemReview(){
    TaskItem(ToDoTask(1, "title", "description",Priority.HIGHT),{})
}