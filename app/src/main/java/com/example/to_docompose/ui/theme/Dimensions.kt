package com.example.to_docompose.ui.theme

import androidx.compose.ui.unit.dp

val LARGE_PADDING = 12.dp
val SMALL_PADDING = 6.dp
val MEDIUM_PADDING = 9.dp

val PRIORITY_INDICATER_SIZE = 12.dp
val TOP_APP_BAR_HEIGHT =56.dp
val TASK_ITEM_ELEVATION =5.dp
val PRIORITY_DROP_DOWN =60.dp