package com.example.to_docompose.ui.screen.task

import androidx.compose.foundation.background
import androidx.compose.foundation.layout.*
import androidx.compose.material.Divider
import androidx.compose.material.MaterialTheme
import androidx.compose.material.OutlinedTextField
import androidx.compose.material.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.to_docompose.R
import com.example.to_docompose.component.PriorityDropDown
import com.example.to_docompose.data.model.Priority
import com.example.to_docompose.ui.theme.LARGE_PADDING
import com.example.to_docompose.ui.theme.MEDIUM_PADDING

@Composable
fun TaskContent(
    title: String,
    onTitleChange: (String) -> Unit,
    isTitleValid:Boolean,
    errorTextTitle:String,
    description: String,
    onDescriptionChange: (String) -> Unit,
    isDescriptionValid:Boolean,
    errorDescriptionTitle:String,
    priority: Priority,
    onPriorityChange: (Priority) -> Unit
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .background(color = MaterialTheme.colors.background)
            .padding(all = LARGE_PADDING)
    ) {
        OutlinedTextField(
            modifier  = Modifier.fillMaxWidth(),
            value = title,
            onValueChange ={
                onTitleChange(it)
                           } ,
            label = { Text(text = stringResource(id = R.string.title)) },
            textStyle = MaterialTheme.typography.body1 ,
            singleLine = true,
            isError = !isTitleValid,

        )

        if (!isTitleValid) {
            Text(
                text = errorTextTitle,
                color = MaterialTheme.colors.error,
                style = MaterialTheme.typography.caption,
                modifier = Modifier.padding(start = 16.dp)
            )
        }

        Divider(modifier = Modifier
            .height(MEDIUM_PADDING),
            color = MaterialTheme.colors.background )

        PriorityDropDown(priority = priority, onPrioritySelected = onPriorityChange )


        Divider(modifier = Modifier
            .height(MEDIUM_PADDING),
            color = MaterialTheme.colors.background )

        if (!isDescriptionValid) {
            Text(
                text = errorDescriptionTitle,
                color = MaterialTheme.colors.error,
                style = MaterialTheme.typography.caption,
                modifier = Modifier.padding(start = 16.dp)
            )
        }
        OutlinedTextField(
            modifier = Modifier
                .fillMaxHeight()
                .fillMaxWidth(),
            value = description,
            onValueChange = {desc ->
                onDescriptionChange(desc)
                            },
            label = { Text(text = stringResource(id = R.string.description))},
            isError = !isDescriptionValid,

        )


    }

}


@Composable
@Preview
fun TaskContentPreview(){

    TaskContent(
        title = "",
        onTitleChange = {},
        isTitleValid = false,
        errorTextTitle = "",
        description ="" ,
        onDescriptionChange ={} ,
        isDescriptionValid = true,
        errorDescriptionTitle = "qwq",
        priority =Priority.HIGHT
    ) {}
}