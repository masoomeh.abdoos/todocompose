package com.example.to_docompose.ui.screen.task

import android.util.Log
import androidx.compose.animation.core.updateTransition
import androidx.compose.material.Icon
import androidx.compose.material.IconButton
import androidx.compose.material.Text
import androidx.compose.material.TopAppBar
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.*
import androidx.compose.runtime.Composable
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.text.style.TextOverflow
import androidx.compose.ui.tooling.preview.Preview
import com.example.to_docompose.R
import com.example.to_docompose.data.model.Priority
import com.example.to_docompose.data.model.ToDoTask
import com.example.to_docompose.ui.screen.list.DeleteAction
import com.example.to_docompose.util.Action

@Composable
fun  TaskActionBar(
    selectedTask: ToDoTask?,
    navigationToListScreen :(Action)->Unit
){

    if(selectedTask == null)
        NewTaskActionBar(navigationToListScreen =navigationToListScreen )
    else
        ExistingTaskActionBar(selectedTask = selectedTask,
            navigationToListScreen = navigationToListScreen )
}

@Composable
fun NewTaskActionBar(
    navigationToListScreen :(Action)->Unit
){
    TopAppBar(
        navigationIcon = {
            BackAction(onBackClick = navigationToListScreen)
        },
        title = {
            Text(
                text = stringResource(id = R.string.add_task),
                color = Color.Gray)
        },
        backgroundColor = Color.Black ,
        actions =
        {
            AddAction(onAddClick = navigationToListScreen)
        }
    )
}


@Composable
fun ExistingTaskActionBar(
    selectedTask:ToDoTask,
    navigationToListScreen :(Action)->Unit
){
    TopAppBar(
        navigationIcon = {
                         CloseAction(onBackClick = navigationToListScreen)
           // BackActionBar(onBackClick = navigationToListScreen)
        },
        title = {
            Text(
                text = selectedTask.title,
                color = Color.Gray,
                maxLines = 1,
                overflow = TextOverflow.Ellipsis
            )
        },
        backgroundColor = Color.Black ,
        actions =
        {
            DeleteAction(onDeleteClick  = navigationToListScreen )
            UpdateAction(onUpdateClick =  navigationToListScreen )
        }
    )
}

@Composable
fun BackAction(
    onBackClick: (Action) ->Unit
){
    IconButton(onClick = {onBackClick(Action.NO_ACTION)}) {
         Icon(imageVector = Icons.Filled.ArrowBack ,
             contentDescription = stringResource(id = R.string.back_action),
             tint = Color.White)
    }
}

@Composable
fun CloseAction(
    onBackClick: (Action) ->Unit
){
    IconButton(onClick = {onBackClick(Action.NO_ACTION)}) {
        Icon(imageVector = Icons.Filled.Close ,
            contentDescription = stringResource(id = R.string.close_action),
            tint = Color.White)
    }
}



@Composable
fun DeleteAction(
    onDeleteClick: (Action) ->Unit
){
    IconButton(onClick = {onDeleteClick(Action.DELETE)}) {
        Icon(imageVector = Icons.Filled.Delete  ,
            contentDescription = stringResource(id = R.string.delete_action),
            tint = Color.White)
    }
}


@Composable
fun UpdateAction(
    onUpdateClick: (Action) ->Unit
){
    IconButton(onClick = {onUpdateClick(Action.UPDATE )}) {
        Icon(imageVector = Icons.Filled.Check ,
            contentDescription = stringResource(id = R.string.update_action),
            tint = Color.White)
    }
}
@Composable
fun AddAction(
    onAddClick: (Action) ->Unit
){
    IconButton(onClick = {

        onAddClick(Action.ADD)
    }) {
        Icon(imageVector = Icons.Filled.Check ,
            contentDescription =  stringResource(id = R.string.add_Action),
            tint = Color.White)
    }
}

@Composable
@Preview
fun NewTaskBar(){

    NewTaskActionBar(navigationToListScreen = {})
}


@Composable
@Preview
fun ExistingTaskBarPreview(){

    ExistingTaskActionBar(
        ToDoTask(
            id =1,
            title = "title",
            description = "description",
            priority = Priority.HIGHT
        ),
        navigationToListScreen = {}
    )
}