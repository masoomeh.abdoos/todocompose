package com.example.to_docompose.util

enum class Action {
    ADD,
    UPDATE,
    DELETE,
    DELETE_ALL,
    UNDO,
    NO_ACTION
}

fun String?.toAction():Action{

    return when{
        this == "ADD" ->{
            Action.ADD
        }

        this == "UPDATE" ->{
            Action.UPDATE

        }

        this == "NO_ACTION" ->{
            Action.NO_ACTION

        }

        this == "DELETE" ->{
            Action.DELETE

        }

        else -> {
            Action.NO_ACTION
        }
    }

}