## TO-DO PROJECT
This GitLab repository contains a sample Android project created for personal learning and experimentation with Jetpack Compose, the modern toolkit for building native UI on Android.

![Project Screenshots](https://gitlab.com/masoomeh.abdoos/todocompose/-/raw/main/screen_sh0ot.jpg)

__Project stack in a nutshell__
* MVVM
* Kotlin
* Navigation Component
* Hilt
* Room
* Jetpack Compose
* Flow
